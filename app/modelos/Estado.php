<?php
    class Estado
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        public function getdatos(){
            $consulta = 'SELECT * FROM estadoorden order by Descripcion asc';
            $this->db->query($consulta);

            $resultado = $this->db->registros();
            return $resultado;
        }

        //guardar tipo proyecto
        public function agregar($datos){
            $consulta = 'INSERT INTO estadoorden (Descripcion) 
                    values(:descripcion)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':descripcion',$datos['descripcion']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //obtener cantidad tipo proyectos por descripcion
        public function obtenercantopcdesc($desc){
            $consulta = 'SELECT * FROM estadoorden WHERE Descripcion=:descripcion';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$desc);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad tipo proyecto por descripcion para editar
        public function obtenercantopcdescnotid($descripcion,$id){
            $consulta = 'SELECT * FROM estadoorden 
                        WHERE Descripcion=:descripcion AND EstadoOrdenId NOT IN(:id)';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);
            $this->db->bind(':id',$id);

            $resultado = $this->db->rowCount();
            return $resultado;
        }


        //editar tipo de proyecto
        public function editar($datos){
            $consulta = 'UPDATE estadoorden set Descripcion = :descripcion
                        where EstadoOrdenId = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':id',$datos['code']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //eliminar tipo proyecto
        public function eliminarreg($datos){
            $consulta = 'DELETE FROM estadoorden WHERE EstadoOrdenId=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['code']);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
        
    }
    