<?php

    class Usuario{
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //metodo para obtener usuario para login
        public function obtenerUsuario($usuario,$pass){
            $consulta = 'SELECT (us.UsuaioId)as codusuario,(us.usuario)as usuario,(us.nombres)as nombre,
                    (us.apellidos)as apellido,(us.tipo_user)as tipo,(us.telefono)as tel, (us.correo) as correo,
                    (us.estado) as estado FROM usuarios us
                    where usuario=:usuario and password=:pass';
            $this->db->query($consulta);
            $this->db->bind(':usuario',$usuario);
            $this->db->bind(':pass',$pass);
            $resultado = $this->db->registro();
            return $resultado;
        }
    }