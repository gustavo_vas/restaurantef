<?php
class Orden
    {
        private $db;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }
        public function getdatos(){
            $consulta = 'SELECT
            `OrdenId`, 
            `TipoOrdenId`,
            `MesaId`,
            `MeseroId`,
            `EstadoOrde0nId`,
            `CantidadCliente`,
            `comentarioAnulacion`
            FROM `orden`
            order by OrdenId;';
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function agregar($datos){
            $consulta = 'INSERT INTO `orden` ( `TipoOrdenId`, `MesaId`, `MeseroId`, `EstadoOrde0nId`, `CantidadCliente`, `comentarioAnulacion`) 
            VALUES (:TipoOrdenId, :mesas, :meseros, :estados, :nclientes, :comentario );';
            $this->db->query($consulta);
            
            //vincular los valores
            $this->db->bind(':TipoOrdenId',(int) $datos['tpordenes']);
            $this->db->bind(':mesas',(int) $datos['mesas']);
            $this->db->bind(':meseros',(int) $datos['meseros']);
            $this->db->bind(':estados',(int) $datos['estados']);
            $this->db->bind(':nclientes',(int) $datos['nclientes']);
            $this->db->bind(':comentario',$datos['comentario']);

            //ejecutar
            if ($this->db->execute()) {
                $this->db->closeCursor();
                return $this->db->lastinsertid(); // retornamos el id para poder hacer un append a la orden en dado caso vuelva a confirmar la orden
            }else{
                return false;
            }
        }

        public function actualizar($datos){
            $consulta = 'UPDATE `orden` SET
	        `TipoOrdenId` = :TipoOrdenId,
	             `MesaId` = :mesas,
	           `MeseroId` = :meseros,
	     `EstadoOrde0nId` = :estados,
	    `CantidadCliente` = :nclientes,
	`comentarioAnulacion` = :comentario;
	where orden.OrdenId = :id';
            $this->db->query($consulta);
            
            //vincular los valores
            $this->db->bind(':TipoOrdenId',(int) $datos['tpordenes']);
            $this->db->bind(':mesas',(int) $datos['mesas']);
            $this->db->bind(':meseros',(int) $datos['meseros']);
            $this->db->bind(':estados',(int) $datos['estados']);
            $this->db->bind(':nclientes',(int) $datos['nclientes']);
            $this->db->bind(':comentario',$datos['comentario']);
            $this->db->bind(':id',$datos['id']);

            //ejecutar
            if ($this->db->execute()) {
                $this->db->closeCursor();
                return $datos['id']; // retornamos el id para poder hacer un append a la orden en dado caso vuelva a confirmar la orden
            }else{
                return false;
            }
        }

        public function obtenerOrdePorId($id){
            
            $consulta = 'SELECT
            `OrdenId`, 
            `TipoOrdenId` as tpordenes,
            `MesaId` as mesas,
            `MeseroId` as meseros,
            `EstadoOrde0nId` as estados,
            `CantidadCliente` as nclientes,
            `comentarioAnulacion` as comentario
            FROM `orden`
            where OrdenId=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);           

            $resultado = $this->db->rowCount();

            return $resultado;
        }

        public function obtenerOrdePorIdDatos($id){
            
            $consulta = 'SELECT
            `OrdenId`, 
            `TipoOrdenId` as tpordenes,
            `MesaId` as mesas,
            `MeseroId` as meseros,
            `EstadoOrde0nId` as estados,
            `CantidadCliente` as nclientes,
            `comentarioAnulacion` as comentario
            FROM `orden`
            where OrdenId=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);           
            $orden = $this->db->registros();
            $orden = json_decode(json_encode($orden), true);
            $resultado = $orden;

            return $resultado;
        }

    }