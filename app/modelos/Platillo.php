<?php
    class Platillo
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //obtener unidades
        public function getdatos(){
            $consulta = 'SELECT (pl.PlatilloId)as PlatilloId,(pl.Descripcion)as Descripcion,(pl.Precio)as Precio, 
            (pl.categoriaId)as categoriaId, (ct.descripcion)as descripcionct 
            FROM platillo pl
            inner join categorias ct on pl.categoriaId = ct.CategoriaId
            order by Descripcion';
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //obtener cantidad unidades por descripcion
        public function obtenercantdesc($descripcion){
            $consulta = 'SELECT * FROM platillo WHERE Descripcion=:descripcion';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad unidades por descripcion para editar
        public function obtenercantdescnotid($descripcion,$id){
            $consulta = 'SELECT * FROM platillo 
                        WHERE Descripcion=:descripcion AND PlatilloId NOT IN(:id)';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);
            $this->db->bind(':id',$id);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //guardar unidad
        public function agregar($datos){
            $consulta = 'INSERT INTO platillo (Precio,Descripcion,categoriaId) 
                    values(:cant,:descripcion,:cat)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cant',$datos['cant']);
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':cat',$datos['cat']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //editar unidad
        public function editar($datos){
            $consulta = 'UPDATE platillo set Precio = :cant,Descripcion = :descripcion,categoriaId = :cat1
                        where PlatilloId = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cant',$datos['cant']);
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':id',$datos['code']);
            $this->db->bind(':cat1',$datos['cat1']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //eliminar unidad
        public function eliminarreg($datos){
            $consulta = 'DELETE FROM platillo WHERE PlatilloId=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['code']);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function tomarPlatillosPorIdOrden($idOrden){
            $consulta = 'select *,detalleorden.comentario as opcion,cantidad as numero from platillo
            inner join detalleorden
            on detalleorden.PlatilloId = platillo.PlatilloId
            where detalleorden.OrdenId =:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$idOrden);
           try {
            $resultado = $this->db->registros();
           } catch (\Throwable $th) {
               throw $th;
           }
            $this->db->closeCursor();
            return $resultado;
        }
        
    }
    