<?php
class detalleorden
    {
        private $db;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }
        public function getdatos($id){
            $consulta = 'SELECT
            `DetalleOrdenId`,
            `PlatilloId`,
            `BebidaId`,
            `cantidad`,
            `comentarioAnulacion`,
            `comentario`,
            `OrdenId`
            FROM `detalleorden`
            WHERE detalleorden.DetalleOrdenId = :id';
            $this->db->query($consulta);
            $this->db->bind(':id',(int) $id);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function agregar($consulta,$idOrden){
            $this->borrarTodoDetalleOrdenPorOrdenId($idOrden); // eliminamos todos los records de una orden pasada
            $this->db->query($consulta);
            //ejecutar
                $this->db->execute();
                $this->db->closeCursor();
                return $this->db->lastinsertid(); //se retorna el id del detalle orden para poder almacenar los opciones del platillo en la tabla correspondiente
            
        }

        public function formarInsertCompleto($datos,$idOrden,$bebidaOplatillo){
            if (!isset($datos['comentarioAnulacion'])) {
                $datos['comentarioAnulacion']="1";
            }
            if (!isset($datos['comentario'])) {
                    if($bebidaOplatillo == "PlatilloId"){
                        $datos['comentario'] = str_replace('"','\"',strval(json_encode($datos["opcion"])));
                    }else{
                        $datos['comentario']="1";
                    }
            }
            
            $consulta = "INSERT INTO `detalleorden` ( `".$bebidaOplatillo."`, `cantidad`, `comentarioAnulacion`, `comentario`, `OrdenId`) 
            VALUES (".$datos[$bebidaOplatillo].", ".$datos['numero'].", ".$datos['comentarioAnulacion'].", \"".$datos['comentario']."\", ".$idOrden." );";
            return $consulta;
        }

        public function borrarTodoDetalleOrdenPorOrdenId($idOrden){
            $consulta = "DELETE FROM `detalleorden` WHERE OrdenId = :idOrden;";

            $this->db->query($consulta);  
            $this->db->bind(':idOrden',(int) $idOrden);
            $this->db->execute();
            $this->db->closeCursor();
    }
}