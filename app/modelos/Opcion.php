<?php 
    class Opcion
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        public function getOpciones(){
            $consulta = 'SELECT * FROM opciones order by Descripcion asc';
            $this->db->query($consulta);

            $resultado = $this->db->registros();
            return $resultado;
        }

        //guardar tipo proyecto
        public function agregaropc($datos){
            $consulta = 'INSERT INTO opciones (Descripcion) 
                    values(:descripcion)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':descripcion',$datos['descripcion']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //obtener cantidad tipo proyectos por descripcion
        public function obtenercantopcdesc($desc){
            $consulta = 'SELECT * FROM opciones WHERE Descripcion=:descripcion';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$desc);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad tipo proyecto por descripcion para editar
        public function obtenercantopcdescnotid($descripcion,$id){
            $consulta = 'SELECT * FROM opciones 
                        WHERE Descripcion=:descripcion AND OpcionId NOT IN(:id)';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);
            $this->db->bind(':id',$id);

            $resultado = $this->db->rowCount();
            return $resultado;
        }


        //editar tipo de proyecto
        public function editaropc($datos){
            $consulta = 'UPDATE opciones set Descripcion = :descripcion
                        where OpcionId = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':id',$datos['code']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //eliminar tipo proyecto
        public function eliminaropc($datos){
            $consulta = 'DELETE FROM opciones WHERE OpcionId=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['code']);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
    
}
