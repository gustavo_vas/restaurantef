<?php
    class Bebida
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }

        //obtener unidades
        public function getdatos(){
            $consulta = 'SELECT (pl.BebidaId)as BebidaId,(pl.Descripcion)as Descripcion,(pl.Precio)as Precio, 
            (pl.categoriaId)as categoriaId, (ct.descripcion)as descripcionct 
            FROM bebidas pl
            inner join categorias ct on pl.categoriaId = ct.CategoriaId
            order by Descripcion';
            
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //obtener cantidad unidades por descripcion
        public function obtenercantdesc($descripcion){
            $consulta = 'SELECT * FROM bebidas WHERE Descripcion=:descripcion';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //obtener cantidad unidades por descripcion para editar
        public function obtenercantdescnotid($descripcion,$id){
            $consulta = 'SELECT * FROM bebidas 
                        WHERE Descripcion=:descripcion AND BebidaId NOT IN(:id)';
            $this->db->query($consulta);

            $this->db->bind(':descripcion',$descripcion);
            $this->db->bind(':id',$id);

            $resultado = $this->db->rowCount();
            return $resultado;
        }

        //guardar unidad
        public function agregar($datos){
            $consulta = 'INSERT INTO bebidas (Precio,Descripcion,categoriaId) 
                    values(:cant,:descripcion,:cat)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cant',$datos['cant']);
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':cat',$datos['cat']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //editar unidad
        public function editar($datos){
            $consulta = 'UPDATE bebidas set Precio = :cant,Descripcion = :descripcion,categoriaId = :cat1
                        where BebidaId = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cant',$datos['cant']);
            $this->db->bind(':descripcion',$datos['descripcion']);
            $this->db->bind(':id',$datos['code']);
            $this->db->bind(':cat1',$datos['cat1']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //eliminar unidad
        public function eliminarreg($datos){
            $consulta = 'DELETE FROM bebidas WHERE BebidaId=:id';
            $this->db->query($consulta);

            $this->db->bind(':id',$datos['code']);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
        
    }
    