<?php
    class Categorias extends Controlador
    {
        public function __construct(){
            date_default_timezone_set('America/El_Salvador');
            $this->categoriaModelo = $this->modelo('Categoria');
            Sesion::start();
        }
        
        public function index(){            
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                $getdata = $this->categoriaModelo->getdatos();
                $datos = [
                    'getdata'=>$getdata
                ];
                $this->vista('/categorias/index',$datos);
            }else{
                redireccionar('/login/destroySesion');
            }
        }

        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $desc = $_POST['descripcion'];
                    $descs = $this->categoriaModelo->obtenercantopcdesc($desc);

                    if ($descs==0) {
                        $datos = [
                            'descripcion' => $_POST['descripcion']
                        ];
    
                        if($this->categoriaModelo->agregar($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function update(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = $_POST['descripcion'];
                    $id = $_POST['code'];
                    $descripcions = $this->categoriaModelo->obtenercantopcdescnotid($descripcion,$id);

                    if ($descripcions==0) {
                        $datos = [
                            'descripcion' => $_POST['descripcion'],
                            'code' => $_POST['code']
                        ];
    
                        if($this->categoriaModelo->editar($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function eliminar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $datos = [
                            'code' => $_POST['idc']
                        ];
    
                        if($this->categoriaModelo->eliminarreg($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }                  
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
    