<?php
    class Errores extends Controlador{

        public function index(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $this->vista('/errores/notfound');
            }else{
                redireccionar('/login/destroySesion');
            }
        }

        //muestra pagina de mantenimineto
        public function mantenimiento(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $this->vista('/errores/mantenimiento');
            }else{
                redireccionar('/login/destroySesion');
            }
        }

        //metodo para destruir la sesion
        public function destroySesion(){
            Sesion::destroy();
            header('Location: '.RUTA_URL);
        }
    }