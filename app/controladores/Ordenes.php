<?php
    class Ordenes extends Controlador
    {
        public function __construct(){
            Sesion::start();   
            $this->tpordenModelo = $this->modelo('Tporden');
            $this->mesaModelo = $this->modelo('Mesa');
            $this->meseroModelo = $this->modelo('Mesero');
            $this->estadoModelo = $this->modelo('Estado');
            $this->bebidaModelo = $this->modelo('Bebida');
            $this->platilloModelo = $this->modelo('Platillo');
            $this->opciones = $this->modelo("opcion");
            $this->ordenesModelo = $this->modelo("orden");
            $this->detalleOrden = $this->modelo("detalleorden");
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $this->vista('/ordenes/index'); // solo devolvera la vista
                /**
                 * Utilizaremos la funcion de tomardatos para poder llenar los inputs con datos de la base
                 */
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function tomardatos(){
          //  if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $tpordenes = $this->tpordenModelo->getTpordenes();
                $mesas = $this->mesaModelo->getdatos();
                $meseros = $this->meseroModelo->getdatos();
                $estados = $this->estadoModelo->getdatos();
                $bebidas = $this->bebidaModelo->getdatos();
                $platillos = $this->platilloModelo->getdatos();
                $opcionesplatillos = $this->opciones->getOpciones();
                $datos = [
                    'tpordenes'=>$tpordenes,
                    'mesas'=>$mesas,
                    'meseros'=>$meseros,
                    'estados'=>$estados,
                    'bebidas'=>$bebidas,
                    'platillos'=>$platillos,
                    'opcionesplatillos'=>$opcionesplatillos
                ];
                header('Access-Control-Allow-Origin: *'); // para que no tenga un error Access-Control-Allow-Origin en el front-end de la vista
                header('Content-Type: application/json'); // retornara un json
                echo json_encode($datos);  // hacemos un echo y formalizamos el archivo json 
           /* }else{
                    redireccionar('/errores/destroySesion');
             }*/ 
        }

        public function obtenerorden(){
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
            header('Content-Type: application/json'); // retornara un json
            $body = file_get_contents('php://input');
            $datosId = (array) json_decode($body);
            $datos = $this->ordenesModelo->obtenerOrdePorIdDatos($datosId["id"]);
            $platillos = $this->platilloModelo->tomarPlatillosPorIdOrden($datosId["id"]);
            $platillos = json_decode(json_encode($platillos), true);
            $platillosEncode = [];
            foreach ($platillos as $index => $valor) {
                $platillosEncode[$index] = $valor;
                $platillosEncode[$index]["opcion"] = json_decode(stripslashes($valor['opcion'])); // se toma el array de tipo "[{\"Descripcion\":\"sopa de pollo\"}]" para poder convertirlo a array propio con stripslashes y json_decode
            }
            $datos[0]["platillosOrden"] = $platillosEncode;
            echo json_encode($datos);  // hacemos un echo y formalizamos el archivo json 
        }

        public function guardar(){
            header('Access-Control-Allow-Origin: *');
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
            header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
           // if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $body = file_get_contents('php://input');
                    $datos = (array) json_decode($body);
                    if (isset($datos['id'])) {
                        $id = $datos['id'];
                        $idOrdenExiste = $this->ordenesModelo->obtenerOrdePorId($id); // se tomara el id de la orden
                        //se retornara siempre el id de la orden a la hora de agregar, asi cuando encuentre una orden
                        //no se agregara pero si se actulizara
                    }else{
                        $idOrdenExiste = 0;
                    }

                    if ($idOrdenExiste==0) {
                        $resultado = $this->ordenesModelo->agregar($datos);
                        if($resultado>0){
                            $this->actualizarDetalleOrden($datos,$resultado);
                            echo $resultado;//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{ // existe por lo tanto se actualizara
                        $resultado = $this->ordenesModelo->actualizar($datos);
                        if($resultado>0){
                            $this->actualizarDetalleOrden($datos,$resultado);
                            echo $resultado; //correcto
                        }else{
                            echo '-1'; //error
                        }
                    }                    
                }else{
                    echo '0';//post no enviado
                }
                
           /* }else{
                redireccionar('/errores/destroySesion');
            }*/
        }

        public function actualizarDetalleOrden($datos,$idOrden){
            $insertCompleto="";
            foreach ($datos["bebidas"] as $key => $value) { // tomar las bebidas y almacenarlas
               $insertCompleto=$this->detalleOrden->formarInsertCompleto(json_decode(json_encode($value), true),$idOrden,"BebidaId");
            $idDetalleOrdenPlatillo = $this->detalleOrden->agregar($insertCompleto,$idOrden);

            }
            foreach ($datos["platillos"] as $key => $value) { // tomar todos los platillos y almacenarlas
                $insertCompleto=$this->detalleOrden->formarInsertCompleto(json_decode(json_encode($value), true),$idOrden,"PlatilloId");
            $this->detalleOrden->agregar($insertCompleto,$idOrden);
            }
        }
        
    }
    