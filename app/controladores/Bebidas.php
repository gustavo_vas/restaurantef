<?php
    class Bebidas extends Controlador
    {
        public function __construct(){
            Sesion::start();            
            $this->bebidaModelo = $this->modelo('Bebida');
            $this->categoriaModelo = $this->modelo('Categoria');
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $getdata = $this->bebidaModelo->getdatos();
                $cate = $this->categoriaModelo->getdatos();
                $datos = [
                    'getdata'=>$getdata,
                    'cate'=>$cate
                ];
                $this->vista('/bebidas/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = $_POST['descripcion'];
                    $descripcions = $this->bebidaModelo->obtenercantdesc($descripcion);

                    if ($descripcions==0) {
                        $datos = [
                            'cant' => $_POST['cant'],
                            'descripcion' => $_POST['descripcion'],
                            'cat' => $_POST['cat']
                        ];
    
                        if($this->bebidaModelo->agregar($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function update(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = $_POST['descripcion'];
                    $id = $_POST['code'];
                    $descripcions = $this->bebidaModelo->obtenercantdescnotid($descripcion,$id);

                    if ($descripcions==0) {
                        $datos = [
                            'cant' => $_POST['cant'],
                            'descripcion' => $_POST['descripcion'],
                            'code' => $_POST['code'],
                            'cat1' => $_POST['cat1']
                        ];
    
                        if($this->bebidaModelo->editar($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function eliminar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $datos = [
                            'code' => $_POST['idc']
                        ];
    
                        if($this->bebidaModelo->eliminarreg($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }                  
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
    