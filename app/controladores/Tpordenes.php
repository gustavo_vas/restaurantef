<?php 
    class Tpordenes extends Controlador
    {
        public function __construct(){
            date_default_timezone_set('America/El_Salvador');
            $this->tpordenModelo = $this->modelo('Tporden');
            Sesion::start();
        }
        
        public function index(){            
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                $tpordenes = $this->tpordenModelo->getTpordenes();
                $datos = [
                    'tpordenes'=>$tpordenes
                ];
                $this->vista('/tpordenes/index',$datos);
            }else{
                redireccionar('/login/destroySesion');
            }
        }

        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $desc = $_POST['descripcion'];
                    $descs = $this->tpordenModelo->obtenercantopcdesc($desc);

                    if ($descs==0) {
                        $datos = [
                            'descripcion' => $_POST['descripcion']
                        ];
    
                        if($this->tpordenModelo->agregaropc($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function update(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = $_POST['descripcion'];
                    $id = $_POST['code'];
                    $descripcions = $this->tpordenModelo->obtenercantopcdescnotid($descripcion,$id);

                    if ($descripcions==0) {
                        $datos = [
                            'descripcion' => $_POST['descripcion'],
                            'code' => $_POST['code']
                        ];
    
                        if($this->tpordenModelo->editaropc($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function eliminar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $datos = [
                            'code' => $_POST['idc']
                        ];
    
                        if($this->tpordenModelo->eliminaropc($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }                  
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
    