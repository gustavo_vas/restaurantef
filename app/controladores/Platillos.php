<?php
    class Platillos extends Controlador
    {
        public function __construct(){
            Sesion::start();            
            $this->platilloModelo = $this->modelo('Platillo');
            $this->categoriaModelo = $this->modelo('Categoria');
        }

        public function index(){
            if (Sesion::getSesion('tipo')==1 && Sesion::getSesion('estado')==1) {
                $getdata = $this->platilloModelo->getdatos();
                $cate = $this->categoriaModelo->getdatos();
                $datos = [
                    'getdata'=>$getdata,
                    'cate'=>$cate
                ];
                $this->vista('/platillos/index',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = $_POST['descripcion'];
                    $descripcions = $this->platilloModelo->obtenercantdesc($descripcion);

                    if ($descripcions==0) {
                        $datos = [
                            'cant' => $_POST['cant'],
                            'descripcion' => $_POST['descripcion'],
                            'cat' => $_POST['cat']
                        ];
    
                        if($this->platilloModelo->agregar($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function update(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = $_POST['descripcion'];
                    $id = $_POST['code'];
                    $descripcions = $this->platilloModelo->obtenercantdescnotid($descripcion,$id);

                    if ($descripcions==0) {
                        $datos = [
                            'cant' => $_POST['cant'],
                            'descripcion' => $_POST['descripcion'],
                            'code' => $_POST['code'],
                            'cat1' => $_POST['cat1']
                        ];
    
                        if($this->platilloModelo->editar($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function eliminar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $datos = [
                            'code' => $_POST['idc']
                        ];
    
                        if($this->platilloModelo->eliminarreg($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }                  
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
    