<?php
    class Administradores extends Controlador{

        public function __construct(){
            date_default_timezone_set('America/El_Salvador'); 
            $this->adminModelo = $this->modelo('Admin');
            Sesion::start();
        }
        
        public function index(){            
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                $this->vista('/administrador/index');
            }else{
                redireccionar('/login/destroySesion');
            }
        }
        
        //metodo para eliminar las sesiones
        public function destroySesion(){
            Sesion::destroy();
            header('Location: '.RUTA_URL);
        }
    }