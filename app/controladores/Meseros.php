<?php
    class Meseros extends Controlador
    {
        public function __construct(){
            date_default_timezone_set('America/El_Salvador');
            $this->meseroModelo = $this->modelo('Mesero');
            Sesion::start();
        }
        
        public function index(){            
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                $meseros = $this->meseroModelo->getdatos();
                $datos = [
                    'meseros'=>$meseros
                ];
                $this->vista('/meseros/index',$datos);
            }else{
                redireccionar('/login/destroySesion');
            }
        }

        public function guardar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $desc = $_POST['descripcion'];
                    $descs = $this->meseroModelo->obtenercantopcdesc($desc);

                    if ($descs==0) {
                        $datos = [
                            'descripcion' => $_POST['descripcion']
                        ];
    
                        if($this->meseroModelo->agregar($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function update(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = $_POST['descripcion'];
                    $id = $_POST['code'];
                    $descripcions = $this->meseroModelo->obtenercantopcdescnotid($descripcion,$id);

                    if ($descripcions==0) {
                        $datos = [
                            'descripcion' => $_POST['descripcion'],
                            'code' => $_POST['code']
                        ];
    
                        if($this->meseroModelo->editar($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }
                        
                    }else{
                        echo '2';//ya existe una cuadrilla con ese nombre
                    }                    
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function eliminar(){
            if(Sesion::getSesion('tipo') == 1 && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $datos = [
                            'code' => $_POST['idc']
                        ];
    
                        if($this->meseroModelo->eliminarreg($datos)){
                            echo '1';//correcto
                        }else{
                            echo '0';//error
                        }                  
                }else{
                    echo '3';//post no enviado
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
    }
    