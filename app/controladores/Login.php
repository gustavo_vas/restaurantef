<?php

    class Login extends Controlador{
        
        public function __construct(){            
            date_default_timezone_set('America/El_Salvador');   
            $this->usuarioModelo = $this->modelo('Usuario');
            Sesion::start();
        }

        public function index(){            
            $this->vista('/Login/inicio');
        }
        
        public function login(){
            //obtener el usuario
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $user=ltrim(rtrim(strip_tags($_POST['usuario'])));
                $clave=ltrim(rtrim(strip_tags($_POST['clave'])));
                if (isset($user) && isset($clave)) {
                    $usuario = $this->usuarioModelo->obtenerUsuario($user,$clave);
                    if($usuario){
                        if($usuario->estado == 1){
                            $this->createSesion('codusuario',$usuario->codusuario);
                            $this->createSesion('usuario',$usuario->usuario);
                            $this->createSesion('tipo',$usuario->tipo);
                            $this->createSesion('estado',$usuario->estado);
                            $this->createSesion('nombreUser',$usuario->nombre);
                            $this->createSesion('apellidoUser',$usuario->apellido);
                            $this->createSesion('correo',$usuario->correo);
                            
                            switch ($usuario->tipo) {
                                case 1:
                                    redireccionar('/Administradores/');
                                break;
                                
                                default:
                                    redireccionar('/login/destroySesion');
                                break;
                            }
                        }else{
                            redireccionar('/login/destroySesion');
                        }   
                    }else{
                        redireccionar('/login/destroySesion');
                    }
                }else{                
                    redireccionar('/login/destroySesion');
                }        
                            
            }else{                
                redireccionar('/login/destroySesion');
            }            
        }

        //metodo para crear las sesiones
        public function createSesion($nombreSesion,$valor){
            Sesion::setSesion($nombreSesion,$valor);
        }

        //metodo para eliminar las sesiones
        public function destroySesion(){
            Sesion::destroy();
            header('Location: '.RUTA_URL);
        }
    }
    