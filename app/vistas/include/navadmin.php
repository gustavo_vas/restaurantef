    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo RUTA_URL;?>/administradores/" class="nav-link">Inicio</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Ayuda</a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Buscar" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                    </button>
                </div>
                </div>
            </form>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a href="<?php echo RUTA_URL; ?>/administradores/destroySesion" class="nav-link">
                    <i class="nav-icon fas fa-sign-out-alt"></i>
                    Salir
                </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <img src="<?php echo RUTA_URL;?>/img/logo.png" alt="Logo" class="brand-image img-circle elevation-3"
                style="opacity: .8">
            <span class="brand-text font-weight-light">Administrador</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block"><?php echo Sesion::getSesion('nombreUser'). ' '. Sesion::getSesion('apellidoUser'); ?></a>
            </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview menu-open">
                <a href="<?php echo RUTA_URL;?>/administradores/" class="nav-link active">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Inicio
                    </p>
                </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            Administración
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">8</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/opciones/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Opciones Platillo</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/tpordenes/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Tipo de Orden</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/mesas/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Mesas</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/estados/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Estado Orden</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/meseros/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Meseros</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/platillos/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Platillos</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/categorias/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Categorias</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/bebidas/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Bebidas</p>
                            </a>
                        </li>
                    </ul>
                </li>

                 <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            Ordenes
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">1</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/ordenes/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Tomar Orden</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            Movimientos
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">1</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/Movimientos/" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Caja</p>
                            </a>
                        </li>
                    </ul>
                </li> 
            </ul>
            </nav>
        </div>
        </aside>