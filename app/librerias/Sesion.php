<?php
    class Sesion{
        //iniciar la sesion
        static function start(){
            @session_start();
        }

        //obtener el valor de uno de los indices de la sesion
        static function getSesion($name){
            return $_SESSION[$name];
        }

        //inicializamos un valor a la variable de sesion
        static function setSesion($name, $data){
            $_SESSION[$name] = $data;
        }

        //destruir la sesion
        static function destroy(){
            @session_destroy();
        }
    }