import Vue from 'vue';
import Notifications from 'vue-notification'
import Vbase from "./components/base.component.vue";
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);
window.axios = require('axios');
Vue.use(Notifications)
Vue.component('v-platillos',require('./components/platillos.component.vue').default);
Vue.component('v-bebidas',require('./components/bebidas.component.vue').default);
Vue.component('v-card',require('./components/card.component.vue').default);
Vue.component('v-form-inline',require('./components/form.inline.component.vue').default);
Vue.component('v-input-icon',require('./components/input.component.vue').default);

const app = new Vue({
    "el":"#app",
    components:{Vbase}
})
