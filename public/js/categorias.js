$(document).ready(function(){
    $('#code').attr('disabled',true);
    $('#e').attr('disabled',true);

    //muestra el modal de agregar tipo de proyecto
    $(document).on('click','#addopcion',function(){
        $('#descripcion').val('');
        $('#desc').html('');
        $('#addopc').modal('show');
    });

    //muestra modal para editar tipo de proyecto
    $(document).on('click','.editopcshow',function(){
        let id = parseInt($(this).parents("tr").find("td").eq(0).text());
        let descripcion = $(this).parents('tr').find('td').eq(1).text();
        $('#code').val(id);
        $('#descripcion1').val(descripcion);
        $('#editopc').modal('show');
    });

    //agrega tipo proyecto
    $(document).on('click','#btnaddopc',function(){
        if (document.getElementById('descripcion').value=='') {
            $('#desc').html('* Este campo es obligatorio');
        }else{
            let descripcion = $('#descripcion').val();
            let url = $('#ruta').val();

            let postData = {
                descripcion:descripcion
            };

            $.post(url+'/categorias/guardar',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Tipo  Registrado Exitosamente');
                        $('#descripcion').val('');
                        $('#addopc').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('La tipo que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    //editar tipo de proyecto
    $(document).on('click','#btneditopc',function(){
        if (document.getElementById('descripcion1').value=='') {
            $('#desc1').html('* Este campo es obligatorio');
        }else{
            let descripcion = $('#descripcion1').val();
            let code = $('#code').val();
            let url = $('#ruta').val();

            let postData = {
                descripcion:descripcion,
                code:code
            };

            $.post(url+'/categorias/update',postData,function(response){
                switch (response) {
                    case '1':
                        toastr.success('Opción  Registrado Exitosamente');
                        $('#descripcion1').val('');
                        $('#editopc').modal('hide');
                        $('#okis').modal('show');
                    break;

                    case '3':
                        toastr.error('Formulario no enviado');
                    break;

                    case '2':
                        toastr.warning('La opción que desea ingresar ya existe.');
                    break;

                    case '0':
                        toastr.warning('No se guardo el registro');
                    break;
                
                    default:
                        break;
                }
                
            });
        }
    });

    $(document).on('click','.delete',function(){
        let idcuad = $(this).parents('tr').find('td').eq(0).text();
       $('#e').val(idcuad);
        $('#elimopc').modal('show');
    });

    $(document).on('click','#borraropc',function(){
        let url = $('#ruta').val();
        let idc = $('#e').val();
        postData = {
            idc:idc
        };
        
        $.post(url+'/categorias/eliminar',postData,function(response){
            if (response == '1') {                
                    toastr.success('Registro eliminado correctamente.');
                    $('#e').val('');
                    $('#elimopc').modal('hide');
                    $('#okis').modal('show');
            }else{
                    toastr.error('Error al eliminar registro, recargue la pagina e intente de nuevo.');
            }
        });
    });
});